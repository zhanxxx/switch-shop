import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

// 人民币=CNY，美元=USD，港币=HKD，英镑=GBP
const store = new Vuex.Store({
  state: {
    mapfiat: {
      CNY: "人民币",
      USD: "美元",
      HKD: "港币",
      GBP: "英镑",
    },
  },
  actions: {
    getMapFiat(state) {
      return state.mapfiat;
    },
  },
  mutations: {},
});

export default store;
