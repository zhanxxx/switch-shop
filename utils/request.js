const curEnv = 0;
const envType = {
  0: {
    BASE_URL: "https://dhd.Money-keep.com",
  },
};
export const base = envType[curEnv];

export const request = function ({
  url,
  method,
  data,
  contentTypeStatus = 1,
  hideloading = false,
}) {
  let token = uni.getStorageSync("token") || "";
  const contentType = {
    1: "application/json",
    2: "application/x-www-form-urlencoded",
  };
  return new Promise((resolve, reject) => {
    if (!hideloading) uni.showLoading({ title: "加載中..." });
    uni.request({
      url: base.BASE_URL + url,
      // url: url,
      header: {
        "content-type": contentType[contentTypeStatus],
        token: token,
      },
      method: method.toUpperCase() || "GET",
      dataType: "jsonp",
      data,
      success: (res) => {
        if (res.data) {
          let data = JSON.parse(res.data);
          if (data.code === 8000 || data.code === 8100) {
            uni.removeStorage({ key: "token" });

            uni.setStorageSync("language", 0);

            uni.navigateTo({
              url: "/pages/public/login/index",
            });
            reject(data);
            return;
          }
          if (data.code !== 0) {
            uni.showToast({
              title: data.msg,
              duration: 2000,
              mask: true,
              icon: "none",
            });
            reject(data);
            return;
          }
          if (!hideloading) uni.hideLoading();
          resolve(data);
        } else {
          resolve({ code: 0, data: {} });
        }
      },
      fail: (err) => {
        if (!hideloading) uni.hideLoading();
        reject(err);
      },
    });
  });
};
