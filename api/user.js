import { request } from "@/utils/request";

// 用户详情
export function userdetail() {
  return request({
    url: "/api/user/detail",
    method: "get",
  });
}
// 申请商家
export function applyBusiness(data) {
  return request({
    url: "/api/user/applyBusiness",
    method: "post",
    data,
  });
}
// 用户资产明细
export function getAmountLogs(data) {
  return request({
    url: "/api/user/getAmountLogs",
    method: "get",
    data,
  });
}
// 用户划转
export function exchange(data) {
  return request({
    url: "/api/user/exchange",
    method: "post",
    data,
  });
}
// 用户转账
export function transfer(data) {
  return request({
    url: "/api/user/transfer",
    method: "post",
    data,
  });
}

// KYC认证
export function kycVerify(data) {
  return request({
    url: "/api/user/kycVerify",
    method: "post",
    data,
  });
}

// 直推
export function directTeam(data) {
  return request({
    url: "/api/user/directTeam",
    method: "get",
    data,
  });
}
// 间推
export function indirectTeam(data) {
  return request({
    url: "/api/user/indirectTeam",
    method: "get",
    data,
  });
}

// 币种列表
export function coinList(data) {
  return request({
    url: "/api/index/coinList",
    method: "get",
    data,
  });
}

export function coinList2(data) {
  return request({
    url: "/api/index/coinList2",
    method: "get",
    data,
  });
}
// 发布订单
export function addGoods(data) {
  return request({
    url: "/api/business/addGoods",
    method: "post",
    data,
  });
}
// 我的发布订单
export function myGoodsList(data) {
  return request({
    url: "/api/business/myGoodsList",
    method: "get",
    data,
  });
}
// 撤销订单
export function cancelGoods(data) {
  return request({
    url: "/api/business/cancelGoods",
    method: "post",
    data,
  });
}

// 众筹订单
export function crowdfundingOrderlist(data) {
  return request({
    url: "/api/crowdfundingOrder/list",
    method: "get",
    data,
  });
}

// 获取收款方式
export function getPaymentMethod() {
  return request({
    url: "/api/user/getPaymentMethod",
    method: "get",
  });
}
// 修改收款方式
export function editPaymentMethod(data) {
  return request({
    url: "/api/user/editPaymentMethod",
    method: "post",
    data,
  });
}

// 更新用户信息
export function useredit(data) {
  return request({
    url: "/api/user/edit",
    method: "post",
    data,
  });
}
