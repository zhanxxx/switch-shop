import { request } from '@/utils/request'

export function getHomeData(){
	 return request({
	        url: '/api/index/getSetting',
	        method: 'get'
	    })
}

export function getCoinList(){
	 return request({
	        url: '/api/index/coinList',
	        method: 'get'
	    })
}


export function getNoticeList(data){
	 return request({
	        url: '/api/noticeType/list',
	        method: 'get',
			data
	    })
}

export function getcrowdfunding(data){
	console.log(data)
	 return request({
	        url: '/api/crowdfunding/list',
	        method: 'get',
			data
	    })
}

export function getcategory(){
	 return request({
	        url: '/api/crowdfunding/category',
	        method: 'get'
			
	    })
}

export function addOrderFunding(data){
	 return request({
	        url: '/api/crowdfundingOrder/add',
	        method: 'post',
			data
	    })
}
export function preOrder(data){
	return request({
		   url: '/api/crowdfundingOrder/preOrder',
		   method: 'post',
		   data
	   })
}
export function payPreOrder(data){
	return request({
		   url: '/api/crowdfundingOrder/payPreOrder',
		   method: 'post',
		   data
	   })
}

//加值

export function recharge(data){
	 return request({
	        url: '/api/user/recharge',
	        method: 'post',
			data
	    })
}


//提现

export function withdraw(data){
	 return request({
	        url: '/api/user/withdraw',
	        method: 'post',
			data
	    })
}

export function homeDialog(){
	 return request({
	        url: '/api/Notice/dialog',
	        method: 'get'
	    })
}