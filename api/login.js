import { request } from '@/utils/request'

// 获取验证码
export function sendEmailCode(data) {
    return request({
        url: '/api/auth/sendEmailCode',
        method: 'post',
        data
    })
}

// 注册
export function authregister(data) {
    return request({
        url: '/api/auth/register',
        method: 'post',
        data
    })
}

// 邮箱登录
export function codelogin(data) {
    return request({
        url: '/api/auth/codelogin',
        method: 'post',
        data
    })
}
// 密码登录
export function pwdlogin(data) {
    return request({
        url: '/api/auth/pwdlogin',
        method: 'post',
        data
    })
}