import { request } from "@/utils/request";
export function getNoticeList(data) {
  return request({
    url: "/api/noticeType/list",
    method: "get",
    data,
  });
}

export function getUserOrder(data) {
  return request({
    url: "/api/business/myGoodsList",
    method: "get",
    data,
  });
}

export function getOrder(data) {
  return request({
    url: "/api/user/businessOrder",
    method: "get",
    data,
  });
}

//获取c2c页面列表
export function getGoodsList(data) {
  return request({
    url: "/api/business/goodsList",
    method: "get",
    data,
  });
}

//买入订单
export function getbuyGoods(data) {
  return request({
    url: "/api/business/buyGoods",
    method: "post",
    data,
  });
}

//出售订单
export function getSellGoods(data) {
  return request({
    url: "/api/business/sellGoods",
    method: "post",
    data,
  });
}

//获取我的订单
export function getMyGoods() {
  return request({
    url: "/api/business/myGoodsList",
    method: "get",
  });
}

//申述
export function appealOrder(data) {
  return request({
    url: "/api/business/appeal",
    method: "post",
    data,
  });
}

// 已付款
export function paid(data) {
  return request({
    url: "/api/business/paid",
    method: "post",
    data,
  });
}

// 已收款
export function confirm(data) {
  return request({
    url: "/api/business/received",
    method: "post",
    data,
  });
}

// 我发布的订单
export function mine(data) {
  return request({
    url: "/api/business/businessOrder",
    method: "get",
    data,
  });
}
