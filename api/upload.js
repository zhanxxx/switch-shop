import { base } from '@/utils/request'

// 上传图片
export function upload(file) {
    const { tempFilePaths, tempFiles } = file
    return new Promise((resolve, reject) => {
        uni.uploadFile({
            url: `${base.BASE_URL}/api/user/uploadImg`,
            // url: '/api/user/uploadImg',
            filePath: tempFilePaths[0],
            name: 'file',
            header: {
                token: uni.getStorageSync("token")
            },
            success(res) {
                console.log("上传成功")
                resolve(res)
            },
            fail(err) {
                console.log("上传失败")
                reject(err)
            }
        })
    })
}

// 上传视频
export function uploadVideo(file) {
    const { tempFilePaths, tempFiles } = file
    return new Promise((resolve, reject) => {
        uni.uploadFile({
            url: `${base.BASE_URL}/api/user/uploadVideo`,
            // url: '/api/user/uploadVideo',
            filePath: tempFilePaths,
            name: 'file',
            header: {
                token: uni.getStorageSync("token")
            },
            success(res) {
                console.log("上传成功")
                resolve(res)
            },
            fail(err) {
                console.log("上传失败")
                reject(err)
            }
        })
    })
}